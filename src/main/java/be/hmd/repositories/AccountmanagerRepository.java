package be.hmd.repositories;


import be.hmd.domain.Accountmanager;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountmanagerRepository extends CrudRepository<Accountmanager, Long> {
    Accountmanager findByEmail(String email);
}