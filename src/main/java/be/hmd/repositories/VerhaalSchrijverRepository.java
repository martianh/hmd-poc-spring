package be.hmd.repositories;

import be.hmd.domain.Verhaalschrijver;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by H_Martial on 15/05/16.
 */
@Repository
public interface VerhaalSchrijverRepository extends CrudRepository<Verhaalschrijver, Long> {
    Verhaalschrijver findByEmail(String email);
}
