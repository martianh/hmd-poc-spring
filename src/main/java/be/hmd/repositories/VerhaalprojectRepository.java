package be.hmd.repositories;

import be.hmd.domain.Verhaalproject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by H_Martial on 15/05/16.
 */
@Repository
public interface VerhaalprojectRepository extends CrudRepository<Verhaalproject, Long>{
    Verhaalproject findByNaam(String naam);
}
