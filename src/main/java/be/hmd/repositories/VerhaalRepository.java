package be.hmd.repositories;

import be.hmd.domain.Verhaal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by H_Martial on 16/05/16.
 */
@Repository
public interface VerhaalRepository extends CrudRepository<Verhaal, Long> {
}
