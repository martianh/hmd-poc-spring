package be.hmd.repositories;

import be.hmd.domain.Foto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
@Repository
public interface FotoRepository extends CrudRepository<Foto, Long> {
    List<Foto> findById(Long id);
}
