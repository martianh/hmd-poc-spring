package be.hmd.repositories;

import be.hmd.domain.Partner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by H_Martial on 15/05/16.
 */
@Repository
public interface PartnerRepository extends CrudRepository<Partner, Long> {
    Partner findByEmail(String email);
}
