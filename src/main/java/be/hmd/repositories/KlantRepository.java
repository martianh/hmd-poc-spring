package be.hmd.repositories;

import be.hmd.domain.Klant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by H_Martial on 17/05/16.
 */
@Repository
public interface KlantRepository extends CrudRepository<Klant, Long> {
    Klant findByEmail(String email);
}
