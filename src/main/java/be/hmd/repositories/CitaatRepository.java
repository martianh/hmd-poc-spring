package be.hmd.repositories;

import be.hmd.domain.Citaat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
@Repository
public interface CitaatRepository extends CrudRepository<Citaat, Long> {
    List<Citaat> findById(Long id);
}
