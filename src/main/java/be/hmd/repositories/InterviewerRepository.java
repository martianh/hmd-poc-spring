package be.hmd.repositories;

import be.hmd.domain.Interviewer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by H_Martial on 15/05/16.
 */
@Repository
public interface InterviewerRepository extends CrudRepository<Interviewer, Long> {
    Interviewer findByEmail(String email);
}
