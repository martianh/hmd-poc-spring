package be.hmd.repositories;

import be.hmd.domain.Eindredacteur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by H_Martial on 15/05/16.
 */
@Repository
public interface EindredacteurRepository extends CrudRepository<Eindredacteur, Long> {
    Eindredacteur findByEmail(String email);
}
