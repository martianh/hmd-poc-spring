package be.hmd.domain;

import javax.persistence.*;
import javax.servlet.http.Part;
import java.util.List;
import java.util.Set;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
@Table(name = "verhaalproject")
public class Verhaalproject {
    private long id;
    private String naam;
    private String status;
    private String type;
    private Klant klant;
    private Verhaalschrijver verhaalschrijver;
    private Interviewer interviewer;
    private Verhaal verhaal;
    private Eindredacteur eindredacteur;
    private Partner partner;
    private List<Citaat> citaten;
    private Set<Foto> foto;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "vp_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "naam", unique = true)
    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @OneToOne(mappedBy="verhaalproject")
    public Klant getKlant() {
        return this.klant;
    }

    public void setKlant(Klant klant) {
        this.klant = klant;
    }

    @OneToOne(mappedBy="verhaalproject")
    public Verhaalschrijver getVerhaalschrijver() {
        return this.verhaalschrijver;
    }

    public void setVerhaalschrijver(Verhaalschrijver verhaalschrijver) {
        this.verhaalschrijver = verhaalschrijver;
    }

    @OneToOne(mappedBy="verhaalproject")
    public Interviewer getInterviewer() {
        return this.interviewer;
    }

    public void setInterviewer(Interviewer interviewer) {
        this.interviewer = interviewer;
    }

    @OneToOne(mappedBy="verhaalproject")
    public Eindredacteur getEindredacteur() {
        return eindredacteur;
    }

    public void setEindredacteur(Eindredacteur eindredacteur) {
        this.eindredacteur = eindredacteur;
    }

    @OneToOne(mappedBy="verhaalproject")
    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    @OneToOne(mappedBy="verhaalproject")
    public Verhaal getVerhaal() {
        return verhaal;
    }

    public void setVerhaal(Verhaal verhaal) {
        this.verhaal = verhaal;
    }

    @OneToMany(mappedBy="verhaalproject")
    public Set<Foto> getFoto() {
        return foto;
    }

    public void setFoto(Set<Foto> foto) {
        this.foto = foto;
    }

    @OneToMany(mappedBy="verhaalproject")
    public List<Citaat> getCitaten() {
        return citaten;
    }

    public void setCitaten(List<Citaat> citaten) {
        this.citaten = citaten;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Verhaalproject that = (Verhaalproject) o;

        if (id != that.id) return false;
        if (naam != null ? !naam.equals(that.naam) : that.naam != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        long result = id;
        result = 31 * result + (naam != null ? naam.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return (int)result;
    }
}
