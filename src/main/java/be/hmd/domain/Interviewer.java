package be.hmd.domain;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Interviewer {
    private long id;
    private String naam;
    private String password;
    private String email;
    private String rol;
    private Interview interview;
    private Verhaalproject verhaalproject;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "interviewer_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "naam")
    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "rol")
    @ColumnDefault("'INTERVIEWER'")
    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Basic
    @Column(name = "email", unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @OneToOne(mappedBy="interviewer")
    public Interview getInterview() {
        return this.interview;
    }

    public void setInterview(Interview interview) {
        this.interview = interview;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalproject getVerhaalproject() {
        return verhaalproject;
    }

    public void setVerhaalproject(Verhaalproject verhaalproject) {
        this.verhaalproject = verhaalproject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Interviewer that = (Interviewer) o;

        if (id != that.id) return false;
        if (naam != null ? !naam.equals(that.naam) : that.naam != null) return false;

        return true;
    }
}
