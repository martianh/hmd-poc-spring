package be.hmd.domain;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Foto {
    private long id;
    private String mediaUrl;
    private Verhaalproject verhaalproject;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "mediaUrl", nullable = true)
    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String MediaUrl) {
        this.mediaUrl = MediaUrl;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalproject getVerhaalproject() {
        return verhaalproject;
    }

    public void setVerhaalproject(Verhaalproject verhaalproject) {
        this.verhaalproject = verhaalproject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Foto foto = (Foto) o;

        if (id != foto.id) return false;
        if (mediaUrl != null ? !mediaUrl.equals(foto.mediaUrl) : foto.mediaUrl != null) return false;

        return true;
    }
}