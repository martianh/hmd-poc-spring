package be.hmd.domain;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Verhaalschrijver {
    private long id;
    private String voornaam;
    private String achternaam;
    private String password;
    private String adres;
    private String woonplaats;
    private String email;
    private String rol;
    private Verhaalproject verhaalproject;
    private Set<Verhaal> verhalen;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "vs_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "voornaam")
    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    @Basic
    @Column(name = "achternaam")
    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "rol")
    @ColumnDefault("'WRITER'")
    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Basic
    @Column(name = "adres")
    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    @Basic
    @Column(name = "woonplaats")
    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalproject getVerhaalproject() {
        return verhaalproject;
    }

    public void setVerhaalproject(Verhaalproject verhaalproject) {
        this.verhaalproject = verhaalproject;
    }

    @OneToMany(mappedBy="verhaalschrijver")
    public Set<Verhaal> getVerhalen() {
        return verhalen;
    }

    public void setVerhalen(Set<Verhaal> verhalen) {
        this.verhalen = verhalen;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Verhaalschrijver that = (Verhaalschrijver) o;

        if (id != that.id) return false;
        if (voornaam != null ? !voornaam.equals(that.voornaam) : that.voornaam != null) return false;
        if (achternaam != null ? !achternaam.equals(that.achternaam) : that.achternaam != null) return false;
        if (adres != null ? !adres.equals(that.adres) : that.adres != null) return false;
        if (woonplaats != null ? !woonplaats.equals(that.woonplaats) : that.woonplaats != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;

        return true;
    }
}
