package be.hmd.domain;

import com.mysql.jdbc.Blob;
import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Interview {
    private Long id;
    private String naam;
    private String document;
    private Interviewer interviewer;

    @Id
    @Column(name = "interview_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "naam")
    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @Basic
    @Column(name = "document")
    @Type(type="text")
    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Interviewer getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(Interviewer interviewer) {
        this.interviewer = interviewer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Interview interview = (Interview) o;

        if (id != interview.id) return false;
        if (naam != null ? !naam.equals(interview.naam) : interview.naam != null) return false;
        if (document != null ? !document.equals(interview.document) : interview.document != null) return false;

        return true;
    }
}
