package be.hmd.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Citaat {
    private long id;
    private String tekst;
    private String naam;
    private Verhaalproject verhaalproject;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "naam")
    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @Basic
    @Column(name = "tekst")
    @Type(type="text")
    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalproject getVerhaalproject() {
        return verhaalproject;
    }

    public void setVerhaalproject(Verhaalproject verhaalproject) {
        this.verhaalproject = verhaalproject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Citaat citaat = (Citaat) o;

        if (id != citaat.id) return false;
        if (tekst != null ? !tekst.equals(citaat.tekst) : citaat.tekst != null) return false;

        return true;
    }
}
