package be.hmd.domain;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
@Table(name = "klant")
public class Klant {
    private long id;
    private String voornaam;
    private String achternaam;
    private String password;
    private String bedrijf;
    private String email;
    private String rol;
    private Verhaalproject verhaalproject;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "klant_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "voornaam")
    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    @Basic
    @Column(name = "achternaam")
    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "rol")
    @ColumnDefault("'CLIENT'")
    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Basic
    @Column(name = "bedrijf")
    public String getBedrijf() {
        return bedrijf;
    }

    public void setBedrijf(String bedrijf) {
        this.bedrijf = bedrijf;
    }

    @Basic
    @Column(name = "email", unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String eMail) {
        this.email = eMail;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalproject getVerhaalproject() {
        return verhaalproject;
    }

    public void setVerhaalproject(Verhaalproject verhaalproject) {
        this.verhaalproject = verhaalproject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Klant klant = (Klant) o;

        if (id != klant.id) return false;
        if (voornaam != null ? !voornaam.equals(klant.voornaam) : klant.voornaam != null) return false;
        if (achternaam != null ? !achternaam.equals(klant.achternaam) : klant.achternaam != null) return false;
        if (bedrijf != null ? !bedrijf.equals(klant.bedrijf) : klant.bedrijf != null) return false;
        if (email != null ? !email.equals(klant.email) : klant.email != null) return false;

        return true;
    }
}
