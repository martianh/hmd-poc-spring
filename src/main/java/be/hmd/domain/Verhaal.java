package be.hmd.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Verhaal {
    private int id;
    private String Document;
    private String status;
    private Verhaalproject verhaalproject;
    private Verhaalschrijver verhaalschrijver;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Type(type = "text")
    @Column(name = "document")
    public String getDocument() {
        return Document;
    }

    public void setDocument(String document) {
        Document = document;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalproject getVerhaalproject() {
        return verhaalproject;
    }

    public void setVerhaalproject(Verhaalproject verhaalproject) {
        this.verhaalproject = verhaalproject;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalschrijver getVerhaalschrijver() {
        return verhaalschrijver;
    }

    public void setVerhaalschrijver(Verhaalschrijver verhaalschrijver) {
        this.verhaalschrijver = verhaalschrijver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Verhaal verhaal = (Verhaal) o;

        if (id != verhaal.id) return false;
        if (status != null ? !status.equals(verhaal.status) : verhaal.status != null) return false;

        return true;
    }
}
