package be.hmd.domain;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Partner {
    private long id;
    private String voornaam;
    private String achternaam;
    private String password;
    private String email;
    private String rol;
    private String groepNaam;
    private Verhaalproject verhaalproject;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "p_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "voornaam")
    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    @Basic
    @Column(name = "achternaam")
    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "email", unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String eMail) {
        this.email = eMail;
    }

    @Basic
    @Column(name = "groepNaam")
    public String getGroepNaam() {
        return groepNaam;
    }

    public void setGroepNaam(String groepNaam) {
        this.groepNaam = groepNaam;
    }

    @Basic
    @Column(name = "rol")
    @ColumnDefault("'PARTNER'")
    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalproject getVerhaalproject() {
        return verhaalproject;
    }

    public void setVerhaalproject(Verhaalproject verhaalproject) {
        this.verhaalproject = verhaalproject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Partner partner = (Partner) o;

        if (id != partner.id) return false;
        if (voornaam != null ? !voornaam.equals(partner.voornaam) : partner.voornaam != null) return false;
        if (achternaam != null ? !achternaam.equals(partner.achternaam) : partner.achternaam != null) return false;
        if (rol != null ? !rol.equals(partner.rol) : partner.rol != null) return false;

        return true;
    }
}
