package be.hmd.domain;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Eindredacteur {
    private Long id;
    private String naam;
    private String password;
    private String rol;
    private String email;
    private Verhaalproject verhaalproject;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "naam")
    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "rol")
    @ColumnDefault("'REDACT'")
    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Verhaalproject getVerhaalproject() {
        return verhaalproject;
    }

    public void setVerhaalproject(Verhaalproject verhaalproject) {
        this.verhaalproject = verhaalproject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Eindredacteur that = (Eindredacteur) o;

        if (id != that.id) return false;
        if (naam != null ? !naam.equals(that.naam) : that.naam != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;

        return true;
    }
}
