package be.hmd.domain;

import javax.persistence.*;

/**
 * Created by H_Martial on 27/04/16.
 */
@Entity
public class Eindproduct {
    private int id;
    private String verhaal;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "verhaal")
    public String getVerhaal() {
        return verhaal;
    }

    public void setVerhaal(String verhaal) {
        this.verhaal = verhaal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Eindproduct that = (Eindproduct) o;

        if (id != that.id) return false;
        if (verhaal != null ? !verhaal.equals(that.verhaal) : that.verhaal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (verhaal != null ? verhaal.hashCode() : 0);
        return result;
    }
}
