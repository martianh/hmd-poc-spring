package be.hmd.controller;

/**
 * Created by mhabi on 4/25/16.
 */
import be.hmd.Rollen;
import be.hmd.domain.*;
import be.hmd.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    private KlantService klantService;
    @Autowired
    private AccountmanagerService accountmanagerService;
    @Autowired
    private VerhaalSchrijverService verhaalSchrijverService;
    @Autowired
    private EindredacteurService eindredacteurService;
    @Autowired
    private InterviewerService interviewerService;
    @Autowired
    private VerhaalprojectService verhaalprojectService;
    @Autowired
    AppService appService;

    @RequestMapping(value="/")
    public String index(Model model) {
        model.addAttribute("title", "Hello My Dear");
        return "home";
    }

    @RequestMapping(value="/home")
    public String userhome(HttpServletRequest request, Model model) {
        String rol = appService.getRol();
        String fullName = appService.getFullName(request, rol);
        Long verhaalprojectID = appService.getVerhaalProjectId(request, rol);
        Verhaalproject verhaalproject = null;

        if (verhaalprojectID != null)
            verhaalproject = verhaalprojectService.findOne(verhaalprojectID);

        Collection authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        model.addAttribute("title", fullName + " :: Hello My Dear");
        model.addAttribute("naam", fullName);
        model.addAttribute("rol", rol);
        if (verhaalprojectID != null && !rol.equals(Rollen.ACCOUNT_MANAGER)) {
            model.addAttribute("projectUrl", "/verhaal-project?id="+verhaalprojectID);
            model.addAttribute("projectText", "Project naam: "+verhaalproject.getNaam());
        }
        if (rol.equals(Rollen.ACCOUNT_MANAGER)) {
            model.addAttribute("projectUrl", "/createProject");
            model.addAttribute("projectText", "Maak Project Aan...");
        }

        return "userhome";
    }

    @RequestMapping(value="/accessDenied")
    public String accessDenied(Model model) {
        model.addAttribute("title", "ACCESS DENIED - Hello My Dear");
        return "denied";
    }

    @RequestMapping(value="/login", method= RequestMethod.GET)
    public String logUserIn(Model model) {
        model.addAttribute("title", "Hello My Dear - login");
        //model.addAttribute("login", new LoginForm());
        return "login";
    }

    @RequestMapping("/create-users")
    @ResponseBody
    public Map createUser() {
        Map responseMap = appService.seedDatabase();

        return responseMap;
    }


}