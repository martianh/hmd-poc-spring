package be.hmd.controller;

import be.hmd.Rollen;
import be.hmd.domain.*;
import be.hmd.service.CitaatService;
import be.hmd.service.FotoService;
import be.hmd.service.PartnerService;
import be.hmd.service.VerhaalprojectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by H_Martial on 15/05/16.
 */
@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private VerhaalprojectService verhaalprojectService;

    @Autowired
    private CitaatService citaatService;

    @Autowired
    private FotoService fotoService;

    @RequestMapping("/addPartner")
    @ResponseBody
    public Partner addPartner(
            @RequestParam(value="voornaam") String voornaam,
            @RequestParam(value="achternaam") String achternaam,
            @RequestParam(value="password") String password,
            @RequestParam(value="email") String email,
            @RequestParam(value="groep") String groepNaam,
            @RequestParam(value="vpID") Long verhaalprojectID) {
        Verhaalproject verhaalproject = verhaalprojectService.findOne(verhaalprojectID);
        Partner partner = new Partner();
        partner.setVoornaam(voornaam);
        partner.setAchternaam(achternaam);
        partner.setPassword(password);
        partner.setEmail(email);
        partner.setGroepNaam(groepNaam);
        partner.setRol(Rollen.PARTNER);
        partner.setVerhaalproject(verhaalproject);
        partnerService.save(partner);
        return partner;
    }

    @RequestMapping("/addCitaat")
    @ResponseBody
    public Map addCitaat(
            @RequestParam(value="email") String email,
            @RequestParam(value="text") String text,
            @RequestParam(value="vpID") Long verhaalprojectID) {
        Map mapCitaat = new HashMap<>();
        try {
            Partner partner = partnerService.findByEmail(email);
            Citaat citaat = new Citaat();
            Verhaalproject verhaalproject = verhaalprojectService.findOne(verhaalprojectID);
            List<Citaat> citaten = verhaalproject.getCitaten();

            String naam = partner.getVoornaam() + " " + partner.getAchternaam();
            citaat.setNaam(naam);
            citaat.setTekst(text);
            citaat.setVerhaalproject(verhaalproject);
            citaten.add(citaat);
            verhaalproject.setCitaten(citaten);
            citaatService.save(citaat);
            verhaalprojectService.save(verhaalproject);

            mapCitaat.put("naam", citaat.getNaam());
            mapCitaat.put("tekst", citaat.getTekst());
            mapCitaat.put("verhaalproject", verhaalproject.getNaam());
        }
        catch (NullPointerException e) {
            mapCitaat.put("Error", "Citaat is niet opgeslagen (missing parameter or wrong email)");
        }

        return mapCitaat;
    }

    @RequestMapping("/addFoto")
    @ResponseBody
    public Map addFoto(
            @RequestParam(value="email") String email,
            @RequestParam(value="foto") String mediaUrl,
            @RequestParam(value="vpID") Long verhaalprojectID) {
        Map mapFoto = new HashMap<>();
        try {
            Foto foto = new Foto();
            Partner partner = partnerService.findByEmail(email);
            Verhaalproject verhaalproject = verhaalprojectService.findOne(verhaalprojectID);
            Set<Foto> fotos = verhaalproject.getFoto();

            String naam = partner.getVoornaam() + " " + partner.getAchternaam();
            foto.setMediaUrl(mediaUrl);
            foto.setVerhaalproject(verhaalproject);
            fotos.add(foto);
            verhaalproject.setFoto(fotos);
            fotoService.save(foto);
            verhaalprojectService.save(verhaalproject);

            mapFoto.put("url", foto.getMediaUrl());
            mapFoto.put("verhaalproject", verhaalproject.getNaam());
        }
        catch (NullPointerException e) {
            mapFoto.put("Error", "Citaat is niet opgeslagen (missing parameter or wrong email)");
        }
        return mapFoto;
    }
}
