package be.hmd.controller;

import be.hmd.PageNotFoundException;
import be.hmd.domain.*;
import be.hmd.form.VerhaalProjectForm;
import be.hmd.service.*;
import javassist.NotFoundException;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by H_Martial on 15/05/16.
 */
@Controller
public class AccountManagerController {
    @Autowired
    VerhaalprojectService verhaalprojectService;
    @Autowired
    InterviewerService interviewerService;
    @Autowired
    KlantService klantService;
    @Autowired
    EindredacteurService eindredacteurService;
    @Autowired
    VerhaalSchrijverService verhaalSchrijverService;
    @Autowired
    AppService appService;

    @RequestMapping("/verhaal-project")
    public String verhaalproject(@RequestParam(value="id") Long verhaalProjectID, HttpServletRequest request, Model model) {
        try {
            Verhaalproject vp = verhaalprojectService.findOne(verhaalProjectID);
            String rol = appService.getRol();
            String fullName = appService.getFullName(request, rol);
            Klant vpKlant = vp.getKlant();
            Verhaalschrijver vpSchrijver = vp.getVerhaalschrijver();
            Interviewer vpInterviewer = vp.getInterviewer();
            Eindredacteur vpEindredacteur = vp.getEindredacteur();

            String klantNaam = vpKlant.getVoornaam() + " " + vpKlant.getAchternaam();
            String verhaalschrijverNaam = vpSchrijver.getVoornaam() + " " + vpSchrijver.getAchternaam();
            String interviewerNaam = vpInterviewer.getNaam();
            String eindredacteurNaam = vpEindredacteur.getNaam();

            model.addAttribute("title", "Hello My Dear - Verhaal Project");
            model.addAttribute("projectNaam", vp.getNaam());
            model.addAttribute("klantNaam", klantNaam);
            model.addAttribute("verhaalschrijverNaam", verhaalschrijverNaam);
            model.addAttribute("interviewerNaam", interviewerNaam);
            model.addAttribute("eindredacteurNaam", eindredacteurNaam);
            model.addAttribute("fullName", fullName);
        }
        catch (NullPointerException e) {
            throw new PageNotFoundException();
        }
        return "verhaalproject";
    }

    @RequestMapping("/verhaal-project/citaten")
    public String verhaalprojectCitaten(@RequestParam(value="id")Long verhaalProjectID, Model model) {
        Verhaalproject vp = verhaalprojectService.findOne(verhaalProjectID);
        vp.getCitaten();
        model.addAttribute("projectNaam", vp.getNaam());
        return "verhaalprojectCitaten";
    }

    @RequestMapping(value="/createProject", method= RequestMethod.GET)
    public String createVerhaalProject(VerhaalProjectForm verhaalProjectForm, HttpServletRequest request, Model model) {
        model.addAttribute("title", "Hello My Dear - nieuw verhaal project");
        String rol = appService.getRol();
        String fullName = appService.getFullName(request, rol);
        model.addAttribute("fullName", fullName);
        return "create-project";
    }

    @RequestMapping(value="/createProject", method= RequestMethod.POST)
    public String createVerhaalProjectSubmit(@Valid VerhaalProjectForm verhaalProjectForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "create-project";
        }
        //model.addAttribute("project", verhaalProjectForm);
        Klant klant = klantService.findByEmail(verhaalProjectForm.getKlantEmail());
        Verhaalschrijver verhaalschrijver = verhaalSchrijverService.findByEmail(verhaalProjectForm.getKlantEmail());
        Interviewer interviewer = interviewerService.findByEmail(verhaalProjectForm.getInterviewerEmail());
        Eindredacteur eindredacteur = eindredacteurService.findByEmail(verhaalProjectForm.getEindredacteurEmail());
        Verhaalproject newVerhaalProject = new Verhaalproject();
        newVerhaalProject.setNaam(verhaalProjectForm.getNaam());
        newVerhaalProject.setType(verhaalProjectForm.getType());
        newVerhaalProject.setStatus("draft");
        newVerhaalProject.setKlant(klant);
        newVerhaalProject.setVerhaalschrijver(verhaalschrijver);
        newVerhaalProject.setInterviewer(interviewer);
        newVerhaalProject.setEindredacteur(eindredacteur);

        Verhaalproject savedVP = verhaalprojectService.save(newVerhaalProject);
        Long vpID = savedVP.getId();
        return "redirect:/verhaal-project?id="+vpID;
    }
}
