package be.hmd.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H_Martial on 18/05/16.
 */
@Controller
public class ErrorHandleController implements ErrorController {
    private static final String PATH = "/error";

    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = PATH)
    public String error(HttpServletRequest request, HttpServletResponse response, Model model) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        Map requestError = errorAttributes.getErrorAttributes(requestAttributes, false);
        Map errorMap = new HashMap<>();
        model.addAttribute("status", response.getStatus());
        model.addAttribute("error", requestError.get("error"));
        model.addAttribute("message", requestError.get("message"));
        return "error";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
