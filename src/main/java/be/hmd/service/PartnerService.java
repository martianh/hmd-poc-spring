package be.hmd.service;

import be.hmd.domain.Partner;

/**
 * Created by H_Martial on 15/05/16.
 */
public interface PartnerService extends GenericService{
    Partner save(Partner partner);
    Partner findByEmail(String email);
    Partner findOne(Long id);
}
