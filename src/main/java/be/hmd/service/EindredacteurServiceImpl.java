package be.hmd.service;

import be.hmd.domain.Eindredacteur;
import be.hmd.repositories.EindredacteurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by H_Martial on 15/05/16.
 */
@Service
@Transactional
public class EindredacteurServiceImpl implements  EindredacteurService{
    @Autowired
    EindredacteurRepository eindredacteurRepository;

    @Override
    public Eindredacteur save(Eindredacteur eindredacteur) {
        Eindredacteur savedER = eindredacteurRepository.save(eindredacteur);
        return savedER;
    }

    @Override
    public Eindredacteur findByEmail(String email) {
        Eindredacteur eindredacteur = eindredacteurRepository.findByEmail(email);
        return eindredacteur;
    }

    @Override
    public Eindredacteur findOne(Long id) {
        Eindredacteur eindredacteur = eindredacteurRepository.findOne(id);
        return eindredacteur;
    }

    @Override
    public Boolean exists(Long id) {
        Boolean entityExists = false;
        if (eindredacteurRepository.exists(id)) {
            entityExists = true;
        }
        else {
            entityExists = false;
        }
        return entityExists;
    }

    @Override
    public void delete(Long id) {
        eindredacteurRepository.delete(id);
    }
}
