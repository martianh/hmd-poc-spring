package be.hmd.service;

import be.hmd.domain.Accountmanager;

/**
 * Created by H_Martial on 15/05/16.
 */
public interface AccountmanagerService extends GenericService {
    Accountmanager saveAccountmanager(Accountmanager accountmanager);
    Accountmanager findByEmail(String email);
    Accountmanager findOne(Long id);
}
