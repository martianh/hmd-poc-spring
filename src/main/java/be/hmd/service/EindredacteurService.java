package be.hmd.service;

import be.hmd.domain.Eindredacteur;

/**
 * Created by H_Martial on 15/05/16.
 */
public interface EindredacteurService extends GenericService {
    Eindredacteur save(Eindredacteur eindredacteur);
    Eindredacteur findByEmail(String email);
    Eindredacteur findOne(Long id);
}
