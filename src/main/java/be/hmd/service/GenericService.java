package be.hmd.service;

/**
 * Created by H_Martial on 16/05/16.
 */
public interface GenericService {
    Boolean exists(Long id);
    void delete(Long id);
}
