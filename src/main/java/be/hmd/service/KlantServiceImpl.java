package be.hmd.service;

import be.hmd.domain.Klant;
import be.hmd.repositories.KlantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by H_Martial on 27/04/16.
 */
@Service
@Transactional
public class KlantServiceImpl implements KlantService{

    @Autowired
    private KlantRepository klantRepository;

    @Override
    public Klant save(Klant klant) {
        Klant savedKlant = klantRepository.save(klant);
        return savedKlant;
    }

    @Override
    public Klant findByEmail(String email) {
        Klant klant = klantRepository.findByEmail(email);
        return klant;
    }

    @Override
    public Klant findOne(Long id) {
        Klant klant = klantRepository.findOne(id);
        return klant;
    }

    @Override
    public Boolean exists(Long id) {
        Boolean entityExists = false;
        if (klantRepository.exists(id)) {
            entityExists = true;
        }
        else {
            entityExists = false;
        }
        return entityExists;
    }

    @Override
    public void delete(Long id) {
        klantRepository.delete(id);
    }
}
