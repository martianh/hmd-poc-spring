package be.hmd.service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by H_Martial on 18/05/16.
 */
public interface AppService {
    String getRol();
    String getFullName(HttpServletRequest request, String rol);
    Long getVerhaalProjectId (HttpServletRequest request, String rol);
    Map seedDatabase();
}
