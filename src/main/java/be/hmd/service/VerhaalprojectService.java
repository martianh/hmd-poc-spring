package be.hmd.service;

import be.hmd.domain.Verhaalproject;

/**
 * Created by H_Martial on 15/05/16.
 */
public interface VerhaalprojectService extends GenericService {
    Verhaalproject findOne(Long id);
    Verhaalproject findByNaam(String naam);
    Verhaalproject save(Verhaalproject verhaalproject);
}
