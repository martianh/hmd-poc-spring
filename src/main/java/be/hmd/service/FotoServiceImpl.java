package be.hmd.service;

import be.hmd.domain.Foto;
import be.hmd.repositories.FotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
@Service
@Transactional
public class FotoServiceImpl implements FotoService {
    @Autowired
    FotoRepository fotoRepository;

    @Override
    public Foto findOne(Long id) {
        Foto foto = fotoRepository.findOne(id);
        return foto;
    }

    @Override
    public List<Foto> findById(Long id) {
        List<Foto> fotos = fotoRepository.findById(id);
        return fotos;
    }

    @Override
    public Foto save(Foto citaat) {
        Foto savedFoto = fotoRepository.save(citaat);
        return savedFoto;
    }
}
