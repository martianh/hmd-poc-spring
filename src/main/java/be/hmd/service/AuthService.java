package be.hmd.service;

import java.util.Map;

/**
 * Created by H_Martial on 17/05/16.
 */
public interface AuthService {
    Map getCredentials(String email);
}
