package be.hmd.service;

import be.hmd.domain.Citaat;

import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
public interface CitaatService {
    Citaat findOne(Long id);
    List<Citaat> findById(Long id);
    Citaat save(Citaat citaat);
}
