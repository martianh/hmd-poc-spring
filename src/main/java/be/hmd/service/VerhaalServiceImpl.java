package be.hmd.service;

import be.hmd.domain.Verhaal;
import be.hmd.repositories.VerhaalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by H_Martial on 16/05/16.
 */
@Service
@Transactional
public class VerhaalServiceImpl implements  VerhaalService {
    @Autowired
    VerhaalRepository verhaalRepository;

    @Override
    public Verhaal save(Verhaal verhaal) {
        Verhaal savedVerhaal = verhaalRepository.save(verhaal);
        return savedVerhaal;
    }

    @Override
    public Verhaal findOne(Long id) {
        Verhaal foundVerhaal = verhaalRepository.findOne(id);
        return foundVerhaal;
    }
}
