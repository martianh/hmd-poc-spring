package be.hmd.service;

import be.hmd.Rollen;
import be.hmd.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H_Martial on 18/05/16.
 */
@Service
public class AppServiceImpl implements AppService {

    @Autowired
    private KlantService klantService;
    @Autowired
    private AccountmanagerService accountmanagerService;
    @Autowired
    private VerhaalSchrijverService verhaalSchrijverService;
    @Autowired
    private EindredacteurService eindredacteurService;
    @Autowired
    private InterviewerService interviewerService;
    @Autowired
    private PartnerService partnerService;
    @Autowired
    private VerhaalprojectService verhaalprojectService;

    @Override
    public String getRol() {
        Collection authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        String rol = authorities.toString()
                .replace("[", "")
                .replace("]", "")
                .trim();
        return rol;
    }

    @Override
    public String getFullName(HttpServletRequest request, String rol) {
        String fullName = new String();
        String remoteUser = request.getRemoteUser();

        Accountmanager accountmanager;
        Klant klant;
        Partner partner;
        Interviewer interviewer;
        Verhaalschrijver verhaalschrijver;
        Eindredacteur eindredacteur;

        if (remoteUser != null && !remoteUser.isEmpty() && rol != null && !rol.isEmpty()) {
            switch (rol) {
                case Rollen.ACCOUNT_MANAGER:
                    accountmanager = accountmanagerService.findByEmail(remoteUser);
                    fullName = accountmanager.getNaam();
                    break;
                case Rollen.EIND_REDACTEUR:
                    eindredacteur = eindredacteurService.findByEmail(remoteUser);
                    fullName = eindredacteur.getNaam();
                    break;
                case Rollen.INTERVIEWER:
                    interviewer = interviewerService.findByEmail(remoteUser);
                    fullName = interviewer.getNaam();
                    break;
                case Rollen.KLANT:
                    klant = klantService.findByEmail(remoteUser);
                    fullName = klant.getVoornaam() + " " + klant.getAchternaam();
                    break;
                case Rollen.PARTNER:
                    partner = partnerService.findByEmail(remoteUser);
                    fullName = partner.getVoornaam() + " " + partner.getAchternaam();
                    break;
                case Rollen.VERHAAL_SCHRIJVER:
                    verhaalschrijver = verhaalSchrijverService.findByEmail(remoteUser);
                    fullName = verhaalschrijver.getVoornaam() + " " + verhaalschrijver.getAchternaam();
                    break;
            }
        }
        else {
            fullName = null;
            throw new NullPointerException("There is no remoteUser or role (has user been authenticated?)");
        }

        return fullName;
    }

    @Override
    public Long getVerhaalProjectId(HttpServletRequest request, String rol) {
        String remoteUser = request.getRemoteUser();
        Long id = 0L;

        Klant klant;
        Partner partner;
        Interviewer interviewer;
        Verhaalschrijver verhaalschrijver;
        Eindredacteur eindredacteur;

        Verhaalproject verhaalproject;
        if (remoteUser != null && !remoteUser.isEmpty() && rol != null && !rol.isEmpty()) {
            try {
                switch (rol) {
                    case Rollen.EIND_REDACTEUR:
                        eindredacteur = eindredacteurService.findByEmail(remoteUser);
                        id = eindredacteur.getVerhaalproject().getId();
                        break;
                    case Rollen.INTERVIEWER:
                        interviewer = interviewerService.findByEmail(remoteUser);
                        id = interviewer.getVerhaalproject().getId();
                        break;
                    case Rollen.KLANT:
                        klant = klantService.findByEmail(remoteUser);
                        id = klant.getVerhaalproject().getId();
                        break;
                    case Rollen.PARTNER:
                        partner = partnerService.findByEmail(remoteUser);
                        id = partner.getVerhaalproject().getId();
                        break;
                    case Rollen.VERHAAL_SCHRIJVER:
                        verhaalschrijver = verhaalSchrijverService.findByEmail(remoteUser);
                        id = verhaalschrijver.getVerhaalproject().getId();
                        break;
                }
            }
            catch (NullPointerException e) {
                id = null;
            }
        }
        else {
            id = null;
            throw new NullPointerException("There is no remoteUser or role (has user been authenticated?)");
        }
        return id;
    }

    public void createUsers(Map responseMap) {
        Klant klant = new Klant();
        klant.setVoornaam("james");
        klant.setAchternaam("jamerson");
        klant.setBedrijf(null);
        klant.setEmail("jj@gmail.com");
        klant.setPassword("azerty");
        klant.setRol(Rollen.KLANT);
        Map klantMap = new HashMap<String, String>();
        klantMap.put("email", klant.getEmail());
        responseMap.put("Klant", klantMap);

        //dummy Account Manager
        Accountmanager accountmanager = new Accountmanager();
        accountmanager.setEmail("am@aol.com");
        accountmanager.setNaam("John Widwick");
        accountmanager.setPassword("azerty");
        accountmanager.setRol(Rollen.ACCOUNT_MANAGER);
        Map amMap = new HashMap<String, String>();
        amMap.put("email", accountmanager.getEmail());
        responseMap.put("Account Manager", amMap);

        //dummy Verhaal Schrijver
        Verhaalschrijver verhaalschrijver = new Verhaalschrijver();
        verhaalschrijver.setVoornaam("Franz");
        verhaalschrijver.setAchternaam("Kafka");
        verhaalschrijver.setAdres("Boulevard Anspach 43");
        verhaalschrijver.setWoonplaats("1000,  Brussel");
        verhaalschrijver.setEmail("f.kafka@live.com");
        verhaalschrijver.setPassword("azerty");
        verhaalschrijver.setRol(Rollen.VERHAAL_SCHRIJVER);
        Map vsMap = new HashMap<String, String>();
        vsMap.put("email", verhaalschrijver.getEmail());
        responseMap.put("Verhaal Schrijver", vsMap);

        //dummy Interviewer
        Interviewer interviewer = new Interviewer();
        interviewer.setEmail("Cadence.l@live.com");
        interviewer.setNaam("Cadence Laurence");
        interviewer.setPassword("azerty");
        interviewer.setRol(Rollen.INTERVIEWER);
        Map interviewerMap = new HashMap<String, String>();
        interviewerMap.put("email", interviewer.getEmail());
        responseMap.put("Interviewer", interviewerMap);

        //dummy Eind redacteur
        Eindredacteur eindredacteur = new Eindredacteur();
        eindredacteur.setEmail("paul.horn@gmail.com");
        eindredacteur.setNaam("Paul Horn");
        eindredacteur.setPassword("azerty");
        eindredacteur.setRol(Rollen.EIND_REDACTEUR);
        Map erMap = new HashMap<String, String>();
        erMap.put("email", eindredacteur.getEmail());
        responseMap.put("Eind-Redacteur", erMap);

        klantService.save(klant);
        accountmanagerService.saveAccountmanager(accountmanager);
        verhaalSchrijverService.save(verhaalschrijver);
        interviewerService.save(interviewer);
        eindredacteurService.save(eindredacteur);
    }

    @Override
    public Map seedDatabase() {
        Map responseMap = new HashMap<String, HashMap<String, String>>();
        Boolean klantExists = klantService.exists(1L);
        Boolean accountmanagerExists = accountmanagerService.exists(1L);
        Boolean verhaalschrijverExists = verhaalSchrijverService.exists(1L);
        Boolean interviewerExists = interviewerService.exists(1L);

        if (klantExists && accountmanagerExists && verhaalschrijverExists && interviewerExists) {
            klantService.delete(1L);
            accountmanagerService.delete(1L);
            verhaalSchrijverService.delete(1L);
            interviewerService.delete(1L);
            createUsers(responseMap);
        }
        else {
            createUsers(responseMap);
        }

        return responseMap;
    }
}
