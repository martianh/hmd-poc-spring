package be.hmd.service;

import be.hmd.domain.Foto;

import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
public interface FotoService {
    Foto findOne(Long id);
    List<Foto> findById(Long id);
    Foto save(Foto foto);
}
