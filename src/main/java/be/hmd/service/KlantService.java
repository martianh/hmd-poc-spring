package be.hmd.service;

import be.hmd.domain.Klant;

/**
 * Created by H_Martial on 27/04/16.
 */
public interface KlantService extends GenericService{
    Klant save(Klant klant);
    Klant findByEmail(String email);
    Klant findOne(Long id);
}
