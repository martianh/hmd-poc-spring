package be.hmd.service;

import be.hmd.domain.Verhaal;

/**
 * Created by H_Martial on 16/05/16.
 */
public interface VerhaalService {
    Verhaal save(Verhaal verhaal);
    Verhaal findOne(Long id);
}
