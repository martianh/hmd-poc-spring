package be.hmd.service;

import be.hmd.domain.Verhaalschrijver;
import be.hmd.repositories.VerhaalSchrijverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by H_Martial on 15/05/16.
 */
@Service
@Transactional
public class VerhaalSchrijverServiceImpl implements VerhaalSchrijverService, AuthService {
    @Autowired
    VerhaalSchrijverRepository verhaalSchrijverRepository;

    @Override
    public Verhaalschrijver save(Verhaalschrijver verhaalschrijver) {
        Verhaalschrijver savedVerhaalSchrijver = verhaalSchrijverRepository.save(verhaalschrijver);
        return savedVerhaalSchrijver;
    }

    @Override
    public Verhaalschrijver findByEmail(String email) {
        Verhaalschrijver verhaalschrijver = verhaalSchrijverRepository.findByEmail(email);
        return verhaalschrijver;
    }

    @Override
    public Verhaalschrijver findOne(Long id) {
        Verhaalschrijver verhaalschrijver = verhaalSchrijverRepository.findOne(id);
        return verhaalschrijver;
    }

    @Override
    public Boolean exists(Long id) {
        Boolean entityExists = false;
        if (verhaalSchrijverRepository.exists(id)) {
            entityExists = true;
        }
        else {
            entityExists = false;
        }
        return entityExists;
    }

    @Override
    public void delete(Long id) {
        verhaalSchrijverRepository.delete(id);
    }

    @Override
    public Map getCredentials(String email) {
        Map credentials = new HashMap<>();
        try {
            Verhaalschrijver vs = verhaalSchrijverRepository.findByEmail(email);
            String naam = vs.getVoornaam() + " " + vs.getAchternaam();
            credentials.put("naam", naam);
            credentials.put("password", vs.getPassword());
            credentials.put("rol", vs.getRol());
        }
        catch (NullPointerException e) {
            credentials.put(null, null);
        }

        return credentials;
    }
}
