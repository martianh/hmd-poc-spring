package be.hmd.service;

import be.hmd.domain.Accountmanager;
import be.hmd.repositories.AccountmanagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Null;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by H_Martial on 27/04/16.
 */
@Service
@Transactional
public class AccountmanagerServiceImpl implements AccountmanagerService, AuthService{

    @Autowired
    private AccountmanagerRepository amRepository;

    @Override
    public Accountmanager saveAccountmanager(Accountmanager accountmanager) {
        amRepository.save(accountmanager);
        return accountmanager;
    }

    @Override
    public Accountmanager findByEmail(String email) {
        Accountmanager accountmanager = amRepository.findByEmail(email);
        return accountmanager;
    }

    @Override
    public Accountmanager findOne(Long id) {
        Accountmanager accountmanager = amRepository.findOne(id);
        return accountmanager;
    }

    @Override
    public Boolean exists(Long id) {
        Boolean entityExists = false;
        if (amRepository.exists(id)) {
            entityExists = true;
        }
        else {
            entityExists = false;
        }
        return entityExists;
    }

    @Override
    public void delete(Long id) {
        amRepository.delete(id);
    }

    @Override
    public Map getCredentials(String email) {
        Map credentials = new HashMap<>();
        try {
            Accountmanager am = amRepository.findByEmail(email);
            credentials.put("naam", am.getNaam());
            credentials.put("password", am.getPassword());
            credentials.put("rol", am.getRol());
        }
        catch (NullPointerException e) {
            credentials.put(null, null);
        }

        return credentials;
    }
}
