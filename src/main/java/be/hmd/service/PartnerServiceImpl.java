package be.hmd.service;

import be.hmd.domain.Partner;
import be.hmd.repositories.PartnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by H_Martial on 15/05/16.
 */

@Service
@Transactional
public class PartnerServiceImpl implements PartnerService{
    @Autowired
    private PartnerRepository partnerRepository;

    @Override
    public Partner save(Partner partner) {
        partnerRepository.save(partner);
        return partner;
    }

    @Override
    public Partner findByEmail(String email) {
        Partner partner = partnerRepository.findByEmail(email);
        return partner;
    }

    @Override
    public Partner findOne(Long id) {
        Partner partner = partnerRepository.findOne(id);
        return partner;
    }

    @Override
    public Boolean exists(Long id) {
        Boolean entityExists = false;
        if (partnerRepository.exists(id)) {
            entityExists = true;
        }
        else {
            entityExists = false;
        }
        return entityExists;
    }

    @Override
    public void delete(Long id) {
        partnerRepository.delete(id);
    }
}
