package be.hmd.service;

import be.hmd.domain.Verhaalschrijver;

/**
 * Created by H_Martial on 15/05/16.
 */
public interface VerhaalSchrijverService extends GenericService {
    Verhaalschrijver save(Verhaalschrijver verhaalschrijver);
    Verhaalschrijver findByEmail(String email);
    Verhaalschrijver findOne(Long id);
}
