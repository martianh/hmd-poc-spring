package be.hmd.service;

import be.hmd.domain.Citaat;
import be.hmd.repositories.CitaatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
@Service
@Transactional
public class CitaatServiceImpl implements CitaatService {
    @Autowired
    CitaatRepository citaatRepository;

    @Override
    public Citaat findOne(Long id) {
        Citaat citaat = citaatRepository.findOne(id);
        return citaat;
    }

    @Override
    public List<Citaat> findById(Long id) {
        List<Citaat> citaten = citaatRepository.findById(id);
        return citaten;
    }

    @Override
    public Citaat save(Citaat citaat) {
        Citaat savedCitaat = citaatRepository.save(citaat);
        return savedCitaat;
    }
}
