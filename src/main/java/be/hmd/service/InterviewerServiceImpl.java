package be.hmd.service;

import be.hmd.domain.Interviewer;
import be.hmd.repositories.InterviewerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by H_Martial on 15/05/16.
 */
@Service
@Transactional
public class InterviewerServiceImpl implements InterviewerService {
    @Autowired
    InterviewerRepository interviewerRepository;

    @Override
    public Interviewer save(Interviewer interviewer) {
        Interviewer savedInterviewer = interviewerRepository.save(interviewer);
        return savedInterviewer;
    }

    @Override
    public Interviewer findByEmail(String email) {
        Interviewer interviewer = interviewerRepository.findByEmail(email);
        return interviewer;
    }

    @Override
    public Interviewer findOne(Long id) {
        Interviewer interviewer = interviewerRepository.findOne(id);
        return interviewer;
    }

    @Override
    public Boolean exists(Long id) {
        Boolean entityExists = false;
        if (interviewerRepository.exists(id)) {
            entityExists = true;
        }
        else {
            entityExists = false;
        }
        return entityExists;
    }

    @Override
    public void delete(Long id) {
        interviewerRepository.delete(id);
    }
}
