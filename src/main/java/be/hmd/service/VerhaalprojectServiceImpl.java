package be.hmd.service;

import be.hmd.domain.Verhaalproject;
import be.hmd.repositories.VerhaalprojectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by H_Martial on 15/05/16.
 */
@Service
@Transactional
public class VerhaalprojectServiceImpl implements VerhaalprojectService{
    @Autowired
    VerhaalprojectRepository verhaalprojectRepository;

    @Override
    public Verhaalproject findOne(Long id) {
        Verhaalproject foundProject = verhaalprojectRepository.findOne(id);
        return foundProject;
    }

    @Override
    public Verhaalproject findByNaam(String naam) {
        Verhaalproject verhaalproject = verhaalprojectRepository.findByNaam(naam);
        return verhaalproject;
    }

    @Override
    public Verhaalproject save(Verhaalproject verhaalproject) {
        Verhaalproject saveVP = verhaalprojectRepository.save(verhaalproject);
        return saveVP;
    }

    @Override
    public Boolean exists(Long id) {
        Boolean entityExists = false;
        if (verhaalprojectRepository.exists(id)) {
            entityExists = true;
        }
        else {
            entityExists = false;
        }
        return entityExists;
    }

    @Override
    public void delete(Long id) {
        verhaalprojectRepository.delete(id);
    }
}
