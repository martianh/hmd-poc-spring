package be.hmd.service;

import be.hmd.domain.Interviewer;

/**
 * Created by H_Martial on 15/05/16.
 */
public interface InterviewerService extends GenericService {
    Interviewer save(Interviewer interviewer);
    Interviewer findByEmail(String email);
    Interviewer findOne(Long id);
}
