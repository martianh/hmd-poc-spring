package be.hmd;

/**
 * Created by H_Martial on 18/05/16.
 */
public final class Rollen {
    private Rollen() {}

    public static final String KLANT = "Klant";

    public static final String VERHAAL_SCHRIJVER = "Verhaal Schrijver";

    public static final String ACCOUNT_MANAGER = "Account Manager";

    public static final String INTERVIEWER = "Interviewer";

    public static final String PARTNER = "Partner";

    public static final String EIND_REDACTEUR = "Eind-Redacteur";
}
