package be.hmd.form;

/**
 * Created by H_Martial on 15/05/16.
 */
public class LoginForm {
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String email;
    private String password;
}
