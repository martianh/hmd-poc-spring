package be.hmd.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by H_Martial on 15/05/16.
 */
public class VerhaalProjectForm {
    @NotNull
    @Size(min = 5, max = 80)
    private String naam;

    @NotNull
    @Pattern(regexp = "(?:(boek|film))")
    private String type;

    @NotNull
    private String klantEmail;

    @NotNull
    private String verhaalschrijverEmail;

    @NotNull
    private String interviewerEmail;

    @NotNull
    private String eindredacteurEmail;


    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKlantEmail() {
        return klantEmail;
    }

    public void setKlantEmail(String klantEmail) {
        this.klantEmail = klantEmail;
    }

    public String getVerhaalschrijverEmail() {
        return verhaalschrijverEmail;
    }

    public void setVerhaalschrijverEmail(String verhaalschrijverEmail) {
        this.verhaalschrijverEmail = verhaalschrijverEmail;
    }

    public String getInterviewerEmail() {
        return interviewerEmail;
    }

    public void setInterviewerEmail(String interviewerEmail) {
        this.interviewerEmail = interviewerEmail;
    }

    public String getEindredacteurEmail() {
        return eindredacteurEmail;
    }

    public void setEindredacteurEmail(String eindredacteurEmail) {
        this.eindredacteurEmail = eindredacteurEmail;
    }
}
