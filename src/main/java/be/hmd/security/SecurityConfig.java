package be.hmd.security;

/**
 * Created by H_Martial on 16/05/16.
 */
import be.hmd.Rollen;
import be.hmd.security.AuthenticationProviders.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=true,prePostEnabled=true,proxyTargetClass=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private AccountmanagerAuthenticationProvider accountmanagerauthProvider;

    @Autowired
    private VerhaalschrijverAuthenticationProvider verhaalschrijverauthProvider;

    @Autowired
    private InterviewerAuthenticationProvider interviewerAuthenticationProvider;

    @Autowired
    private KlantAuthenticationProvider klantAuthenticationProvider;

    @Autowired
    private PartnerAuthenticationProvider partnerAuthenticationProvider;

    @Autowired
    private EindRedacteurAuthenticationProvider eindRedacteurAuthenticationProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(accountmanagerauthProvider);
        auth.authenticationProvider(verhaalschrijverauthProvider);
        auth.authenticationProvider(interviewerAuthenticationProvider);
        auth.authenticationProvider(klantAuthenticationProvider);
        auth.authenticationProvider(partnerAuthenticationProvider);
        auth.authenticationProvider(eindRedacteurAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/home")
                .failureUrl("/login?login_error=1")
                .and()
                .logout().logoutSuccessUrl("/")
                .and()
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/login*").permitAll()
                .antMatchers("/logout*").permitAll()
                .antMatchers("/logoutSuccess*").permitAll()
                .antMatchers("/accessDenied*").permitAll()
                .antMatchers("/create-users*").permitAll()
                .antMatchers("/api/**").permitAll()
                .antMatchers("/createProject*").hasAuthority(Rollen.ACCOUNT_MANAGER)
                .antMatchers("/**").authenticated()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied");
    }
}
