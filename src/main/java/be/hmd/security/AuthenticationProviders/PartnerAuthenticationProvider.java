package be.hmd.security.AuthenticationProviders;

import be.hmd.Rollen;
import be.hmd.domain.Interviewer;
import be.hmd.domain.Partner;
import be.hmd.service.InterviewerService;
import be.hmd.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
@Component
public class PartnerAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private PartnerService partnerService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            Partner partner = partnerService.findByEmail(authentication.getName());
            String name = partner.getEmail();//authentication.getName();
            String password = authentication.getCredentials().toString();
            if (password.equals(partner.getPassword())) {
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                grantedAuths.add(new SimpleGrantedAuthority(Rollen.PARTNER));
                Authentication auth = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
                return auth;
            } else {
                return null;
            }
        }
        catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

