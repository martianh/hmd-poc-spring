package be.hmd.security.AuthenticationProviders;

import be.hmd.Rollen;
import be.hmd.domain.Accountmanager;
import be.hmd.domain.Verhaalschrijver;
import be.hmd.service.AccountmanagerService;
import be.hmd.service.VerhaalSchrijverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
@Component
public class VerhaalschrijverAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private VerhaalSchrijverService verhaalSchrijverService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            Verhaalschrijver verhaalschrijver = verhaalSchrijverService.findByEmail(authentication.getName());
            String name = verhaalschrijver.getEmail();//authentication.getName();
            String password = authentication.getCredentials().toString();
            if (password.equals(verhaalschrijver.getPassword())) {
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                grantedAuths.add(new SimpleGrantedAuthority(Rollen.VERHAAL_SCHRIJVER));
                Authentication auth = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
                return auth;
            } else {
                return null;
            }
        }
        catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

