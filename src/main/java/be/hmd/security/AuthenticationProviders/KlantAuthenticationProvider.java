package be.hmd.security.AuthenticationProviders;

import be.hmd.Rollen;
import be.hmd.domain.Interviewer;
import be.hmd.domain.Klant;
import be.hmd.service.InterviewerService;
import be.hmd.service.KlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by H_Martial on 16/05/16.
 */
@Component
public class KlantAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private KlantService klantService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            Klant klant = klantService.findByEmail(authentication.getName());
            String name = klant.getEmail();//authentication.getName();
            String password = authentication.getCredentials().toString();
            if (password.equals(klant.getPassword())) {
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                grantedAuths.add(new SimpleGrantedAuthority(Rollen.KLANT));
                Authentication auth = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
                return auth;
            } else {
                return null;
            }
        }
        catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

