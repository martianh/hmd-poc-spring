package be.hmd.entityTest;

import be.hmd.domain.*;
import be.hmd.service.VerhaalprojectServiceImpl;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by H_Martial on 17/05/16.
 */
public class VerhaalprojectServiceTest {

    @Test
    public void testingMakeProject() {
        Verhaalproject verhaalproject = makeProject();
        assertTrue("saved project naam is 'Test Project'","Test Project".equals(verhaalproject.getNaam()));
        assertTrue("saved project Klant voornaam is 'james'","james".equals(verhaalproject.getKlant().getVoornaam()));
        assertTrue("saved project verhaalschrijver achternaam is 'Kafka'","Kafka".equals(verhaalproject.getVerhaalschrijver().getAchternaam()));
        assertTrue("saved project Interviewer passwood is 'special","special".equals(verhaalproject.getInterviewer().getPassword()));
    }

    public Verhaalproject makeProject() {
        VerhaalprojectServiceImpl verhaalprojectService = new VerhaalprojectServiceImpl();
        //dummy klant
        Klant klant = new Klant();
        klant.setVoornaam("james");
        klant.setAchternaam("jamerson");
        klant.setBedrijf(null);
        klant.setEmail("jj@gmail.com");
        klant.setPassword("azerty");

        //dummy Verhaal Schrijver
        Verhaalschrijver verhaalschrijver = new Verhaalschrijver();
        verhaalschrijver.setVoornaam("Franz");
        verhaalschrijver.setAchternaam("Kafka");
        verhaalschrijver.setAdres("Boulevard Anspach 43");
        verhaalschrijver.setWoonplaats("1000,  Brussel");
        verhaalschrijver.setEmail("f.kafka@live.com");
        verhaalschrijver.setPassword("azerty");

        //dummy Interviewer
        Interviewer interviewer = new Interviewer();
        interviewer.setEmail("Cadence.l@live.com");
        interviewer.setNaam("Cadence Laurence");
        interviewer.setPassword("special");

        Verhaalproject newVerhaalProject = new Verhaalproject();
        newVerhaalProject.setNaam("Test Project");
        newVerhaalProject.setType("film");
        newVerhaalProject.setStatus("draft");
        newVerhaalProject.setKlant(klant);
        newVerhaalProject.setVerhaalschrijver(verhaalschrijver);
        newVerhaalProject.setInterviewer(interviewer);

        return newVerhaalProject;
    }
}